<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Sexo;
use App\Rol;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\UserFormRequest;
use App\Http\Requests\UserUpdateFormRequest;


class UserController extends Controller
{
       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $users = User::all();
        $sexos = Sexo::all();

        $desde=$request->desde;
        $hasta=$request->hasta;

        if(count($request->all())>1)
        {
            //dd($request->all());
            $sql = User::select('users.*');

            if($request->sexo_id)
            {
                $sql = $sql->whereSexo_id($request->sexo_id);
            }
            if($request->desde)
            {
                $sql = $sql->whereDate('created_at','>=',$request->desde);
            }
            if($request->hasta)
            {
                $sql = $sql->whereDate('created_at','<=',$request->hasta);
            }


            $users=$sql->orderBy('created_at','desc')->get();
            $sexo_id=$request->sexo_id;


        }
        else
        {
            $sexo_id=null;

            $users=User::orderBy('created_at','desc')->get();


        }

        return view('user.index',[
            "users"                 =>  $users,
            "sexo_id"               =>  $sexo_id,
            "sexos"                 =>  $sexos,
            "desde"                 =>  $desde,
            "hasta"                 =>  $hasta,

            ]);








    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sexo=Sexo::all();
        $roles=Rol::all();

        return view("user.create", ["sexo"=>$sexo, "roles"=>$roles]);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserFormRequest $request)
    {
        $user = new User;
        $user->apellido=$request->get('apellido');
        $user->password=bcrypt($request->password);
        $user->name=$request->get('name');
        $user->dni=$request->get('dni');
        $user->domicilio=$request->get('domicilio');
        $user->email=$request->get('email');
        $user->telefono=$request->get('telefono');
        $user->fecha_nac=$request->get('fecha_nac');
        $user->cuit=$request->get('cuit');
        $user->sexo_id=$request->get('sexo_id');
        $user->rol_id=$request->get('rol_id');

        $user->save();

        Session::flash('store_user','La persona '.$user->apellido. ' ha sido registrda con éxito');
        return Redirect::to('user');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user=User::findOrFail($id);
        $sexo=Sexo::all();
        $roles=Rol::all();

        return view("user.edit",["user"=>$user,"sexo"=>$sexo,"roles"=>$roles]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateFormRequest $request, $id)
    {
        $user=User::findOrFail($id);
        $user->apellido=$request->get('apellido');
        $user->password=$request->get('password');
        $user->name=$request->get('name');
        $user->dni=$request->get('dni');
        $user->domicilio=$request->get('domicilio');
        $user->email=$request->get('email');
        $user->telefono=$request->get('telefono');
        $user->fecha_nac=$request->get('fecha_nac');
        $user->cuit=$request->get('cuit');
        $user->sexo_id=$request->get('sexo_id');
        $user->rol_id=$request->get('rol_id');

        $user->update();
        Session::flash('update_user','La persona '.$user->apellido. ' ha sido actualizada con éxito');
        return Redirect::to('user');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::findOrFail($id);
        try{
            User::destroy($id);
            $user->update();
            Session::flash('delete_user','La persona '.$user->apellido. ' ha sido dado de baja correctamente');
            return Redirect::to('user');
        }
        catch(\Illuminate\Database\QueryException $e){
            Session::flash('delete_user_error','La persona '.$user->apellido. ' no puede ser dada de baja');
            return Redirect::to('user');

        }
    }
}
