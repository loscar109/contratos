<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contrato;
use App\Objeto;
use App\Solicitante;
use App\Estado;
use App\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ContratoFormRequest;

use App\Http\Requests\ContratoUpdateFormRequest;

class ContratoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $contratos = Contrato::all();
        $estados=Estado::all();

        $desde=$request->desde;
        $hasta=$request->hasta;

        if(count($request->all())>1)
        {
            //dd($request->all());
            $sql = Contrato::select('contratos.*');

            if($request->estado_id)
            {
                $sql = $sql->whereEstado_id($request->estado_id);
            }
            if($request->desde)
            {
                $sql = $sql->whereDate('created_at','>=',$request->desde);
            }
            if($request->hasta)
            {
                $sql = $sql->whereDate('created_at','<=',$request->hasta);
            }


            $contratos=$sql->orderBy('created_at','desc')->get();
            $estado_id=$request->estado_id;


        }
        else
        {
            $estado_id=null;

            $contratos=Contrato::orderBy('created_at','desc')->get();


        }

        return view('contrato.index',[
            "contratos"             =>  $contratos,
            "estado_id"             =>  $estado_id,
            "estados"               =>  $estados,
            "desde"                 =>  $desde,
            "hasta"                 =>  $hasta,

            ]);




    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        $objetos = Objeto::all();
        $solicitantes = Solicitante::all();
        $estados = Estado::all();

        return view("contrato.create",[
            "users"         =>  $users,
            "objetos"       =>  $objetos,
            "solicitantes"  =>  $solicitantes,
            "estados"       =>  $estados

            ]);




    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContratoFormRequest $request)
    {
        $contrato = new Contrato;
        $contrato->fecha_desde=$request->get('fecha_desde');
        $contrato->fecha_hasta=$request->get('fecha_hasta');
        $contrato->fecha_disp=$request->get('fecha_disp');
        $contrato->nro_disp=$request->get('nro_disp');
        $contrato->resumen=$request->get('resumen');
        $contrato->nombre_archivo=$request->get('nombre_archivo');

        if($request->anexo)
        {
            $file = $request->anexo;
            $file->move(public_path().'/imagenes/anexo/',$file->getClientOriginalName());
            $contrato->anexo=$file->getClientOriginalName();
        }







        $contrato->contrato=$request->get('contrato');
        $contrato->estado_id=$request->get('estado_id');
        $contrato->solicitante_id=$request->get('solicitante_id');
        $contrato->objeto_id=$request->get('objeto_id');
        $contrato->user_id=$request->get('user_id');

        $contrato->save();

        Session::flash('store_contrato','El contrato '.$contrato->resumen. ' se creó con éxito');
        return Redirect::to('contrato');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contrato=Contrato::findOrFail($id);
        $estado=Estado::all();
        $solicitante=Solicitante::all();
        $objeto=Objeto::all();
        $user=User::all();

        return view("contrato.edit",[
            "contrato"      =>  $contrato,
            "estado"        =>  $estado,
            "solicitante"   =>  $solicitante,
            "objeto"        =>  $objeto,
            "user"          =>  $user,
            ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContratoUpdateFormRequest $request, $id)
    {


        $contrato=Contrato::findOrFail($id);
        $contrato->fecha_desde=$request->get('fecha_desde');
        $contrato->fecha_hasta=$request->get('fecha_hasta');
        $contrato->fecha_disp=$request->get('fecha_disp');
        $contrato->nro_disp=$request->get('nro_disp');
        $contrato->resumen=$request->get('resumen');
        $contrato->nombre_archivo=$request->get('nombre_archivo');

        if ($request->hasFile('anexo'))
        {
            $file = $request->anexo;
            $file->move(public_path().'/imagenes/anexo/',$file->getClientOriginalName());
            $contrato->anexo=$file->getClientOriginalName();
        }




        $contrato->contrato=$request->get('contrato');
        $contrato->estado_id=$request->get('estado_id');
        $contrato->solicitante_id=$request->get('solicitante_id');
        $contrato->objeto_id=$request->get('objeto_id');
        $contrato->user_id=$request->get('user_id');
        $contrato->update();
        Session::flash('update_contrato','El contrato '.$contrato->contrato. ' ha sido actualizado con éxito');
        return Redirect::to('contrato');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contrato=Contrato::findOrFail($id);
        try{
            Contrato::destroy($id);
            $contrato->update();
            Session::flash('delete_delete_contrato','El contrato '.$contrato->resumen. ' ha sido eliminado correctamente');
            return Redirect::to('contrato');
        }
        catch(\Illuminate\Database\QueryException $e){
            Session::flash('delete_delete_contrato_error','El contrato '.$contrato->resumen. ' no puede ser eliminado');
            return Redirect::to('contrato');

        }
    }
}
