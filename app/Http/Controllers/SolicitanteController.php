<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Solicitante;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\SolicitanteFormRequest;



class SolicitanteController extends Controller
{
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $solicitantes = Solicitante::all();
        return view('solicitante.index',["solicitantes"=> $solicitantes]);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view("solicitante.create");


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SolicitanteFormRequest $request)
    {
        $solicitante = new Solicitante;
        $solicitante->nombre=$request->get('nombre');
        $solicitante->save();

        Session::flash('store_solicitante','El solicitante '.$solicitante->nombre. ' se creó con éxito');
        return Redirect::to('solicitante');    

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $solicitante=Solicitante::findOrFail($id);
        return view("solicitante.edit",["solicitante"=>$solicitante]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $solicitante=Solicitante::findOrFail($id);
        $solicitante->nombre=$request->get('nombre');
        $solicitante->update();
        Session::flash('update_solicitante','El solicitante '.$solicitante->nombre. ' ha sido actualizado con éxito');
        return Redirect::to('solicitante'); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $solicitante=Solicitante::findOrFail($id);
        try{
            Solicitante::destroy($id);
            $solicitante->update();
            Session::flash('delete_solicitante','El solicitante '.$solicitante->nombre. ' ha sido eliminado correctamente');
            return Redirect::to('solicitante');
        }
        catch(\Illuminate\Database\QueryException $e){
            Session::flash('delete_solicitante_error','El solicitante '.$solicitante->nombre. ' no puede ser eliminado');
            return Redirect::to('solicitante');

        }
    }

}
