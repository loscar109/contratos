<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estado;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\EstadoFormRequest;


class EstadoController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estados = Estado::all();
        return view('estado.index',["estados"=> $estados]);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view("estado.create");


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EstadoFormRequest $request)
    {
        $estado = new Estado;
        $estado->estado=$request->get('estado');
        $estado->save();

        Session::flash('store_estado','El estado '.$estado->estado. ' se creó con éxito');
        return Redirect::to('estado');    

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estado=Estado::findOrFail($id);
        return view("estado.edit",["estado"=>$estado]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $estado=Estado::findOrFail($id);
        $estado->estado=$request->get('estado');
        $estado->update();
        Session::flash('update_estado','El estado '.$estado->estado. ' ha sido actualizado con éxito');
        return Redirect::to('estado'); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $estado=Estado::findOrFail($id);
        try{
            Estado::destroy($id);
            $estado->update();
            Session::flash('delete_estado','El estado '.$estado->estado. ' ha sido eliminado correctamente');
            return Redirect::to('estado');
        }
        catch(\Illuminate\Database\QueryException $e){
            Session::flash('delete_estado_error','El estado '.$estado->estado. ' no puede ser eliminado');
            return Redirect::to('estado');

        }
    }
}
