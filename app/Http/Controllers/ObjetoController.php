<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Objeto;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ObjetoFormRequest;

class ObjetoController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objetos = Objeto::all();
        return view('objeto.index',["objetos"=> $objetos]);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view("objeto.create");


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ObjetoFormRequest $request)
    {
        $objeto = new Objeto;
        $objeto->descripcion=$request->get('descripcion');
        $objeto->save();

        Session::flash('store_objeto','El objeto '.$objeto->descripcion. ' se creó con éxito');
        return Redirect::to('objeto');    

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $objeto=Objeto::findOrFail($id);
        return view("objeto.edit",["objeto"=>$objeto]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objeto=Objeto::findOrFail($id);
        $objeto->descripcion=$request->get('descripcion');
        $objeto->update();
        Session::flash('update_objeto','El objeto '.$objeto->descripcion. ' ha sido actualizado con éxito');
        return Redirect::to('objeto'); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objeto=Objeto::findOrFail($id);
        try{
            Objeto::destroy($id);
            $objeto->update();
            Session::flash('delete_objeto','El objeto '.$objeto->descripcion. ' ha sido eliminado correctamente');
            return Redirect::to('objeto');
        }
        catch(\Illuminate\Database\QueryException $e){
            Session::flash('delete_objeto_error','El objeto '.$objeto->descripcion. ' no puede ser eliminado');
            return Redirect::to('objeto');

        }
    }

}
