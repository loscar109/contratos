<?php

namespace App\Http\Controllers;
use App\Contrato;
use App\User;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    
    public function index()
    {
        $contratos = Contrato::all();
        $cantContratos = $contratos->count();

        $users = User::all();
        $cantUser = $users->count();        

        $contratoActivo=Contrato::where('estado_id','=','1')->count();
        $contratoReservado=Contrato::where('estado_id','=','3')->count();

        return view('welcome',[
            "cantContratos"     => $cantContratos,
            "cantUser"          => $cantUser,
            "contratoActivo"    => $contratoActivo,
            "contratoReservado" => $contratoReservado,

        ]);
    }

}
