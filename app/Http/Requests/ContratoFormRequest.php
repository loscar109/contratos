<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContratoFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fecha_desde'   => 'required',
            'fecha_hasta'   => 'required',
            'fecha_disp'    => 'required',
            'nro_disp'      => 'required',
            'resumen'       => 'required',
            'nombre_archivo'=> 'required',
            'contrato'      => 'required',
            'estado_id'     => 'required',
            'solicitante_id'=> 'required',
            'objeto_id'     => 'required',
            'user_id'    => 'required',
            'anexo'         => 'image|mimes:jpeg,png,jpg,gif,svg|max:4048',

        ];
    }

    public function messages()
    {
        return [
            'fecha_desde.required'      =>  'La fecha desde es requerida',
            'fecha_hasta.required'      =>  'La fecha hasta es requerida',
            'fecha_disp.required'       =>  'La fecha de disposición es requerida',
            'nro_disp.required'         =>  'EL número de disposición es requerido',
            'resumen.required'          =>  'EL resumen es requerido',
            'nombre_archivo.required'   =>  'El nombre de archivo es requerido',
            'fecha_carga.required'      =>  'La fecha de carga es requerida',
            'anexo.image'               =>  'El anexo debe ser una imagen',
            'contrato.required'         =>  'El contrato es requerido',
            'estado_id.required'        =>  'El estado es requerido',
            'solicitante_id.required'   =>  'El solicitante es requerido',
            'objeto_id.required'        =>  'El objeto es requerido',
            'user_id.required'          =>  'La persona es requerida',


        ];
    }
}
