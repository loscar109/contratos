<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class User extends Authenticatable implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public $timestamps=true;

    use Notifiable;

    protected $table = 'users';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'apellido',
        'dni',
        'domicilio',
        'telefono',
        'fecha_nac',
        'cuit',
        'sexo_id',
        'rol_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

       /*HAS es si tiene el id el otro
    BELONG es si el id lo tengo yo*/

    //Una persona tiene un sexo asociado
    public function sexo()
    {
        return $this->belongsTo('App\Sexo');
    }

     //Una persona tiene un rol asociado
     public function rol()
     {
         return $this->belongsTo('App\Rol');
     }

    //Una persona puede tener muchos contratos asociados
    public function contratos()
    {
        return $this->hasMany('App\Contrato');
    }

    public function nombre_completo()
    {
        return $this->name . " " . $this->apellido;
    }

}
