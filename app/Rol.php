<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Rol extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $timestamps=false;

    protected $fillable = ['rol'];

    protected $table = 'roles';

    /*HAS es si tiene el id el otro
    BELONG es si el id lo tengo yo*/

    //Un rol puede tener asociadas muchas personas
    public function users()
    {
        return $this->hasMany('App\User');
    }
}
