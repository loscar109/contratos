<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Solicitante extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $timestamps=false;

    protected $fillable = ['nombre'];

    protected $table = 'solicitantes';


     //Un Solicitante puede tener un contrato
     public function contratos()
     {
         return $this->belongsTo('App\Contratos');
     }




}


