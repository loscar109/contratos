<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Objeto extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $timestamps=false;

    protected $fillable = ['descripcion'];

    protected $table = 'objetos';


    /*HAS es si tiene el id el otro
    BELONG es si el id lo tengo yo*/

    //Un objeto  puede tener muchos contratos asociados
    public function contratos()
    {
        return $this->hasMany('App\Contrato');
    }
}
