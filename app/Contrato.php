<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Contrato extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $timestamps=true;

    protected $fillable = [
        'fecha_desde',
        'fecha_hasta',
        'fecha_disp',
        'nro_disp',
        'resumen',
        'nombre_archivo',
        'anexo',
        'contrato',
        'estado_id',
        'solicitante_id',
        'objeto_id',
        'user_id'
    ];

    protected $table = 'contratos';

    /*HAS es si tiene el id el otro
    BELONG es si el id lo tengo yo*/

    //Un contrato tiene una persona asociada
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    //Un contrato puede estar en un solo estado
    public function estado()
    {
        return $this->belongsTo('App\Estado');
    }

    //Un contrato puede tener solo un solicitante
    public function solicitante()
    {
        return $this->belongsTo('App\Solicitante');
    }

    //Un contrato puede tener solo un objeto
    public function objeto()
    {
        return $this->belongsTo('App\Objeto');
    }


}
