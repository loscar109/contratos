@extends('layouts.admin')
@section('titulo')
<div class="box-header" style="text-align:center">
    <a href="{{ asset('/') }}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>
</div>
@endsection

@section('content')
    <div class="box-body">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:#D2D6DE">
            @include('errors.request')
            @include('user.mensaje')
            <div class="box">
                <div class="box-header">
                    <h4 class="box-title" >
                        <i class="fa fa-id-card" aria-hidden="true"></i> Indice de Personas
                    </h4>
                    <div class="box-tools">
                        <a href= {{ route('user.create')}}>
                            <button class="btn btn-warning">
                                <i class="fa fa-plus"></i> Nuevo
                            </button>
                        </a>

                    </div>
                </div>
                <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="box collapsed-box">
                            <div class="box-header with-border">
                                <i class="fa fa-filter" aria-hidden="true"></i><h3 class="box-title">Filtrar</h3>

                              <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="desplegar">
                                  <i class="fa fa-plus"></i></button>
                              </div>
                            </div>
                            <div class="box-body" style="display: none;">

                                @include('user.search') <!-- elemento de la -->



                            </div>

                        </div>

                        <table id="tablaDetalle" style="border:1px solid black; width:100%" class="table table-bordered table-condensed table-hover">
                            <thead style="background-color:#222D32">
                                <tr>
                                    <th width="20%" style="color:#F8F9F9" height="15px"><p class="text-uppercase" style="font-size:120%">Codigo de Persona</p></th>
                                    <th width="10%" style="color:#F8F9F9" height="15px"><p class="text-uppercase" style="font-size:120%">Persona</p></th>
                                    <th width="15%" style="color:#F8F9F9" height="15px"><p class="text-uppercase" style="font-size:120%">Fecha de Alta</p></th>
                                    <th width="10%" style="color:#F8F9F9" height="15px"><p class="text-uppercase" style="font-size:120%">Opciones</p></th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                <tr onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)">
                                    <td width="20%"><p style="font-size:120%">{{ $user->id }}</p></td>
                                    <td><p style="font-size:120%">{{ $user->nombre_completo() }}</p></td>
                                    <td width="15%"><p style="font-size:120%">{{Carbon\Carbon::parse($user->created_at)->format('d/m/Y') }}</p></td>
                                    <td style="text-align: center" colspan="3">
                                        <a data-backdrop="static" data-keyboard="false" data-target="#modal-delete-{{ $user->id }}" data-toggle="modal">
                                            <button title="eliminar" class="btn btn-warning btn-responsive">
                                                <i class="fa fa-trash"></i> Eliminar
                                            </button>
                                        </a>
                                        <a href="{{URL::action('UserController@edit',$user->id)}}">
                                            <button title="editar" class="btn btn-warning btn-responsive">
                                                <i class="fa fa-edit"></i> Modificar
                                            </button>
                                        </a>
                                        <a data-keyboard="false" data-target="#modal-show-{{ $user->id }}" data-toggle="modal">
                                            <button title="editar" class="btn btn-warning btn-responsive">
                                                <i class="fa fa-eye"></i> Detalle
                                            </button>

                                        </a>

                                        @include('user.modalshow')

                                    </td>
                                </tr>
                                @include('user.modaldelete')
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
    <script src="{{asset('js/tablaDetalle.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){

            $("#sexo_id").select2({
                placeholder:'-Seleccione un sexo-',
                width: '100%',
            });






    });

</script>

    @endpush
    @endsection

