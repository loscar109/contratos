@extends('layouts.admin')
@section('titulo')
<div class="box-header" style="text-align:center">
    <a href="{{ asset('user') }}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>
</div>
@endsection

@section('content')


{!!Form::open(array(
    'url'=>'user',
    'method'=>'POST',
    'autocomplete'=>'off',
))!!}

{{Form::token()}}
<div class="box-body">
    @include('errors.request')
    @include('user.mensaje')
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:#D2D6DE">

        <div class="box">
            <div class="box-header">
                <h4 class="box-title" >
                    <i class="fa fa-id-card" aria-hidden="true"></i> Dar de alta una persona
                </h4>
            </div>
            <div class="box-body">
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="apellido">
                                Apellido
                        </label>
                        <input
                            type="string"
                            name="apellido"
                            maxlength="30"
                            value="{{old('apellido')}}"
                            class="form-control"
                            placeholder="Ingrese el apellido..."
                            title="Introduzca un apellido"
                            >
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="name">
                                Nombres
                        </label>
                        <input
                            type="string"
                            name="name"
                            maxlength="30"
                            value="{{old('name')}}"
                            class="form-control"
                            placeholder="Ingrese el nombre..."
                            title="Introduzca el nombre"
                            >
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="dni">
                            Documento
                        </label>
                        <input
                            type="number"
                            name="dni"
                            min="11111111"
                            max="99999999"

                            value="{{old('dni')}}"
                            class="form-control"
                            placeholder="33.222.111"
                            title="Introduzca el DNI"
                            >
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="domicilio">
                                Domicilio
                        </label>
                        <input
                            type="string"
                            name="domicilio"
                            maxlength="30"

                            value="{{old('domicilio')}}"
                            class="form-control"
                            placeholder="Ingrese el domicilio..."
                            title="Ingrese el domicilio de la persona"
                            >
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="sexo_id">
                            Sexo
                        </label>
                        <select
                            name="sexo_id"
                            id="sexo_id"
                            class="sexo_id form-control"
                            required
                            >
                            <option
                                value="0"
                                disabled="true"
                                selected="true"
                                title="-Seleccione un sexo-"
                                >
                                -Seleccione un sexo-
                            </option>
                            @foreach ($sexo as $s)
                                <option
                                    value="{{$s->id }}">{{$s->descripcion}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="telefono">
                            Teléfono
                        </label>
                        <input
                            type="number"
                            name="telefono"
                            value="{{old('telefono')}}"
                            class="form-control"
                            placeholder="22111222..."
                            title="Introduzca un número de teléfono"
                            >
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="fecha_nac">
                                Fecha de nacimiento
                        </label>
                        <input
                            type="date"
                            name="fecha_nac"
                            min="1900-12-12"
                            value="{{old('fecha_nac')}}"
                            class="form-control"
                            placeholder="dd/mm/YYYY..."
                            title="Introduzca una fecha de nacimiento"
                            >
                    </div>
                </div>



                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="cuit">
                                Cuit
                        </label>
                        <input
                            type="text"
                            name="cuit"
                            value="{{old('cuit')}}"
                            class="form-control"
                            placeholder="22-82736453-11..."
                            title="Introduzca un cuit"
                            >
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="email">
                            Correo electónico
                        </label>
                        <input
                            type="string"
                            name="email"
                            value="{{old('email')}}"
                            class="form-control"
                            placeholder="unemail@mail.com..."
                            title="Introduzca una dirección de correo"
                            >
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="password">
                            Contraseña
                        </label>
                        <input
                            type="password"
                            name="password"
                            min="8"
                            maxlength="13"
                            value="{{old('password')}}"
                            class="form-control"
                            placeholder="********..."
                            title="Introduzca una contraseña"
                            >
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="rol_id">
                            Rol
                        </label>
                        <select
                            name="rol_id"
                            id="rol_id"
                            class="rol_id form-control"
                            required
                            >
                            <option
                                value="0"
                                disabled="true"
                                selected="true"
                                title="-Seleccione un rol-"
                                >
                                -Seleccione un rol-
                            </option>
                            @foreach ($roles as $r)
                                <option
                                    value="{{$r->id }}">{{$r->nombre}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label>

                        </label>
                        <br>
                        <button title="Guardar" class="btn btn-primary btn-responsive" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                        <button title="Limpiar" class="btn btn-danger btn-responsive" type="reset"><i class="fa fa-remove"></i> Cancelar</button>
                    </div>
                </div>

             </div>
            </div>
        </div>
    </div>
    {!!Form::close()!!}

    @push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){

            $("#sexo_id").select2({
                placeholder:'-Seleccione un sexo-',
                width: '100%',
            });

            $("#rol_id").select2({
                placeholder:'-Seleccione un rol-',
                width: '100%',
            });





    });

</script>
@endpush

@endsection

