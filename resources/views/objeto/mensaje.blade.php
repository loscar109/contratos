@if (Session::has('delete_objeto_error'))
        <div class="alert alert-danger"data-auto-dismiss role="alert">{{ Session::get('delete_objeto_error') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif


@if (Session::has('delete_objeto'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('delete_objeto') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('store_objeto'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('store_objeto') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('update_objeto'))
        <div class="alert alert-warning"data-auto-dismiss role="alert">{{ Session::get('update_objeto') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif
