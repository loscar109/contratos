@extends('layouts.admin')

@section('content')
<div class="box-body">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:#D2D6DE">
        @include('errors.request')
        <div class="box">
            <div class="box-header">
                <h4 class="box-title" >
                    <i class="fa fa-key" aria-hidden="true"></i> Iniciar Sesión
                </h4>
            </div>
            <div class="box-body">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                        <div class="form-group">
                            <label for="email"></label>
                            <input 
                                id="email"
                                type="email"
                                class="form-control"
                                name="email" 
                                value="{{ old('email') }}"
                                required
                                autocomplete="email"
                                autofocus
                            >
                        </div>
                        <div class="form-group">
                            <label for="password"></label>
                            <input 
                                id="password"
                                type="password"
                                class="form-control"
                                name="password" 
                                value="{{ old('password') }}"
                                required
                                autocomplete="password"
                                autofocus
                            >
                        </div>
                        <div class="form-group" style="text-align: right">
                            <button type="submit" class="btn btn-warning btn-lg">
                                {{ __('Login') }}
                            </button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
