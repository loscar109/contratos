<!DOCTYPE html>
<html lang="es">
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>SisContrato</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

      <!-- Bootstrap 3.3.5 -->
      <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{asset('css/skin-blue.css')}}">
      <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet" href="{{asset('css/dataTables.bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{asset('css/botonDefault.css')}}">
      <link rel="stylesheet" href="{{asset('css/botonGris.css')}}">
      <link rel="stylesheet" href="{{asset('css/botonDanger.css')}}">

      <!-- Font Awesome -->
      <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
      <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />

      <!-- Theme style -->
      <link rel="stylesheet" href="{{asset('css/AdminLTE.css')}}">
      <link rel="stylesheet" href="{{asset('css/_all-skins.css')}}">
      <link rel="apple-touch-icon" href="{{asset('img/apple-touch-icon.png')}}">
      <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">

  </head>

  <body class="skin-yellow">
    <header class="main-header">
      <a href="/" class="logo">
        <span class="logo-mini"><i class="fa fa-papper-o" ></i></span>
        <span class="logo-lg"><b style="font-size:150%">SisContrato</b></span>
      </a>
      <nav class="navbar navbar-static-top" role="navigation">
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">

          @if(Auth::user())
            <li class="dropdown messages-menu">
              <a
                href="/logout"
                title="pantalla principal"
                >
                <i class="fas fa-sign-out-alt" aria-hidden="true"></i> Cerrar sesión
              </a>
            </li>
          @endif
        </ul>
        </div>
      </nav>
    </header>


    <aside class="main-sidebar">
      <section class="sidebar">
        <ul class="sidebar-menu">
          @if(Auth::user())
            @if(Auth::user()->rol->id==2)
              <li>
                <a href="{{ route('contrato.index') }}">
                  <i class="fa fa-handshake" aria-hidden="true"></i><span style="font-size:150%">Contrato</span>
                </a>
              </li>
              <li>
                <a href="{{ route('user.index') }}">
                  <i class="fa fa-id-card" aria-hidden="true"></i><span style="font-size:150%">Persona</span>
                </a>
              </li>
            @endif
            @if(Auth::user()->rol->id==1)
              <li>
                <a href="{{ route('solicitante.index') }}">
                  <i class="fa fa-user" ></i><span style="font-size:150%">Solicitante</span>
                </a>

              <li>
                <a href="{{ route('objeto.index') }}">
                  <i class="fa fa-cog" aria-hidden="true"></i><span style="font-size:150%">Objeto</span>
                </a>
              </li>
              <li>
                <a href="{{ route('estado.index') }}">
                  <i class="fa fa-spinner" aria-hidden="true"></i><span style="font-size:150%">Estado</span>
                </a>
              </li>
              <li>
                <a href="{{ route('sexo.index') }}">
                  <i class="fa fa-venus-mars" aria-hidden="true"></i><span style="font-size:150%">Sexo</span>
                </a>
              </li>
            @endif
            @if(Auth::user()->rol->id==3)
            <li>
              <a href="{{ route('audits.index') }}">
                <i class="fa fa-history" aria-hidden="true"></i><span style="font-size:150%">Auditoría</span>
              </a>
            </li>
            @endif
          @endif
        </ul>
      </section>
    </aside>

    <div class="content-wrapper">
      <section class="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="box">
                  @yield('titulo')
                  @yield('content')
              </div>
            </div>
          </div>
        </section>
      </div>


    <script src="{{asset('js/jquery-3.3.1.js')}}"></script>
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script>


    @stack('scripts')
    <!-- Bootstrap 3.3.5 -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>
    <script src="{{asset('js/toastr.min.js')}}"></script>

    <!-- AdminLTE App -->
    <script src="{{asset('js/app.min.js')}}"></script>

 </body>
</html>
