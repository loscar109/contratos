@if (Session::has('delete_solicitante_error'))
        <div class="alert alert-danger"data-auto-dismiss role="alert">{{ Session::get('delete_solicitante_error') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif


@if (Session::has('delete_solicitante'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('delete_solicitante') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('store_solicitante'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('store_solicitante') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('update_solicitante'))
        <div class="alert alert-warning"data-auto-dismiss role="alert">{{ Session::get('update_solicitante') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif
