@extends('layouts.admin')

@section('titulo')
<div class="box-header" style="text-align:center">
    <a href="{{ asset('solicitante') }}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>
</div>
@endsection

@section('content')

    {!!Form::model($solicitante, [
        'method'=>'PATCH',
        'route'=>['solicitante.update',$solicitante->id]
    ])!!}
    

    {{Form::token()}}
    <div class="box-body">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:#D2D6DE">
            @include('errors.request')
            @include('solicitante.mensaje')
            <div class="box">
                <div class="box-header">
                    <h4 class="box-title" >
                        <i class="fa fa-user" aria-hidden="true"></i> Editar un Solicitante
                    </h4>
                </div>
                <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                            <label for="nombre" class="control-label">Nombre</label>
        
                                <input id="nombre" type="string" class="form-control" name="nombre" value="{{ $solicitante->nombre }}" required>
        
            
                        </div>
        
                

                
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                        <div class="form-group">
                            <button title="Guardar" class="btn btn-primary btn-responsive" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                            <button title="Limpiar" class="btn btn-danger btn-responsive" type="reset"><i class="fa fa-remove"></i> Cancelar</button>
                        </div>
                    </div>
    
                 </div>
                </div>
            </div>   
        </div>
        {!!Form::close()!!}


@endsection
