@extends('layouts.admin')
@section('titulo')
<div class="box-header" style="text-align: center">
    @if(Auth::user()->sexo->id==2)
      <h4 class = "box-title" style="font-size:120%">Bienvenido: <b style="font-size:120%"> {{ Auth::user()->name . " " . Auth::user()->apellido }}</b></h4>
    @endif
    @if(Auth::user()->sexo->id==1)
    <h4 class = "box-title" style="font-size:120%">Bienvenida: <b style="font-size:120%"> {{ Auth::user()->name . " " . Auth::user()->apellido }}</b></h4>
  @endif
    <div class="box-tools pull-right"> </div>
</div>
@endsection


@section('content')
<div class="box-body">
  <div class="box-body">
    @if(Auth::user()->rol->id==3)
        <script>window.location = "{{ route('audits.index') }}";</script>
    @endif

    @if(Auth::user()->rol->id==1)
        <script>window.location = "{{ route('solicitante.index') }}";</script>
    @endif


    @if(Auth::user()->rol->id==2)
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="box box-solid box-default">
        <div class="box-body">
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
              </ol>
              <div class="carousel-inner">
                <div class="item active">
                  <img src={{ asset('fondo/carousel1.jpg') }} height="300px" width="700px">
                  <div class="carousel-caption">
                    <p style="color:white; font-size:300%; background-color:black"><b>Confia en nosotros</b></p>
                  </div>
                </div>
                <div class="item">
                  <img src={{ asset('fondo/carousel2.jpg') }} height="300px" width="700px">
                  <div class="carousel-caption">
                    <p style="color:white; font-size:300%; background-color:black"><b>Nos avalan las mejores compañías</b></p>
                  </div>
                </div>
                <div class="item">
                  <img src={{ asset('fondo/carousel3.jpg') }} height="300px" width="700px">
                  <div class="carousel-caption">
                    <p style="color:white; font-size:300%; background-color:black"><b>Sabemos lo que hacemos</b></p>
                  </div>
                </div>
              </div>
              <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                <span class="fa fa-angle-left"></span>
              </a>
              <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                <span class="fa fa-angle-right"></span>
              </a>
            </div>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="row">
              <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                  <div class="inner">
                    <h3>{{$cantContratos}}</h3>

                    <p style="font-size:150%">Contratos registrados</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-handshake" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
              <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                  <div class="inner">
                    <h3>{{$cantUser}}</h3>

                    <p style="font-size:150%">Personas registradas</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-id-card" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                  <div class="inner">
                    <h3>{{$contratoActivo}}</h3>

                    <p style="font-size:150%">Contratos Activos</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-check" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
              <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                  <div class="inner">
                    <h3>{{$contratoReservado}}</h3>

                    <p style="font-size:150%">Contratos Reservados</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-warning" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endif

  </div>
</div>

@endsection
