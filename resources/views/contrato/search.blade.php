{!! Form::model(Request::only(
    ['estado_id','desde','hasta']),
    ['url' => 'contrato/', 'method'=>'GET', 'autocomplete'=>'on', 'role'=>'search']

    )!!}

<div class="col-lg-2 col-sm-2 col-md-2 col-xs-2 ">
    <label for="estado_id">Estado</label>
    <div class="form-group">
        <select
            name="estado_id"
            id="estado_id"
            class="estado_id form-control"
            >

                @foreach ($estados as $estado)
                    <option
                        value="{{$estado->id}}"
                        @if($estado_id!=null && $estado_id==$estado->id)
                            selected
                        @endif
                    >
                    {{$estado->estado}}
                    </option>
                @endforeach

                <option
                    value="0"
                    @if($estado_id == null || $estado_id==0)
                        selected
                    @endif
                >
                    -- Todas los estados --
                </option>
        </select>
    </div>
</div>


<div class="col-lg-2 col-sm-2 col-md-2 col-xs-2 ">
    <label for="desde">Fecha Desde</label>
    <input
        type="date"
        name="desde"
        id="desde"
        class="fecha form-control"
        value="{{$desde}}"
    >
</div>


<div class="col-lg-2 col-sm-2 col-md-2 col-xs-2 ">
    <label for="hasta">Fecha Hasta</label>
    <input
        type="date"
        name="hasta"
        id="hasta"
        class="fecha form-control"
        value="{{$hasta}}"
    >
</div>


<div class="col-lg-2 col-sm-2 col-md-2 col-xs-2 ">
    <label for=""></label>
    <div class="form-group">
        <span class="input-group-btn">
            <button
                title="buscar"
                type="submit"
                id="bt_add"
                name="filtrar"
                class="btn btn-primary btn-responsive">
                    <i class="fa fa-filter"></i> Filtrar
            </button>

            <a

            href= "{{ route('contrato.index') }}"
            class="btn btn-default"
            >
            <i class="fas fa-eraser"></i>
                ... Limpiar
        </a>

        </span>
    </div>
</div>





{{Form::close()}}

@push('scripts')
<script type="text/javascript">
$(document).ready(function(){


    //si existe un cambio en Fecha Desde
    $('#desde').change(function() {

        //Se captura su valor
        var desde = $(this).val();
        console.log(desde, 'Se cambio la fecha DESDE')
        //y establesco ese valor capturado como minimo en Fecha Hasta
        $('#hasta').attr({"min" : desde});;


        });

    //si existe un cambio en Fecha Hasta
    $('#hasta').change(function() {

        //Se captura su valor
        var hasta = $(this).val();
        console.log(hasta, 'Se cambio la fecha HASTA');

        //si ese valor es diferente a nulo (osea si hay algo dentro)
        if (hasta != "")
        {
            //se deshabilita el desde (para evitar que desde sea mayor que hasta)
            $("#desde").prop('disabled', true);

        }



      });

     //si se clickea en "FIltrar"
      $('#bt_add').click(function () {
        //se debe refrescar (si es que hubo) la prop disabled de desde
        $("#desde").prop('disabled', false);


      });


});







</script>
@endpush
