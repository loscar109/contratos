{{--ventanita modal cuando se haga clic en eliminar--}}


<div class="modal fade modal-slide-in-right"
    aria-hidden="true"
    role="dialog"
    tabindex="-1"
    id="modal-show-{{$contrato->id}}">



    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#F39C12">
                <h3 class="modal-title" style="color: white"><i style="color: white" class="fa fa-exclamation-triangle" aria-hidden="true"></i> Detalle del contrato: {{ $contrato->id }}</h3>
                <div class="modal-body">
                    <table class="table table-bordered table-condensed table-hover">
                        <thead>
                            <tr>
                                <th>Fecha desde</th>
                                <td>{{Carbon\Carbon::parse($contrato->fecha_desde)->format('d/m/Y') }}</td>
                            </tr>
                            <tr>
                                <th>Fecha hasta</th>
                                <td>{{Carbon\Carbon::parse($contrato->fecha_hasta)->format('d/m/Y') }}</td>
                            </tr>
                            <tr>
                                <th>Fecha disposición</th>
                                <td>{{Carbon\Carbon::parse($contrato->fecha_disp)->format('d/m/Y') }}</td>
                            </tr>
                            <tr>
                                <th>Número de disposición</th>
                                <td>{{ $contrato->nro_disp }}</td>

                            </tr>
                            <tr>
                                <th>Resumen</th>
                                <td>{{ $contrato->resumen }}</td>

                            </tr>
                            <tr>
                                <th>Nombre de archivo</th>
                                <td>{{ $contrato->nombre_archivo }}</td>

                            </tr>
                            <tr>
                                <th>Fecha de carga</th>
                                <td>{{Carbon\Carbon::parse($contrato->created_at)->format('d/m/Y') }}</td>

                            </tr>
                            <tr>
                                <th>Anexo</th>
                                <td><img
                                        class="img-thumbnail"
                                        src="{{asset('/imagenes/anexo/'.$contrato->anexo)}}"
                                        height="250px"
                                        width="250px"
                                        alt="sin anexo"
                                        >


                            </tr>
                            <tr>
                                <th>Contrato</th>
                                <td>{{ $contrato->contrato }}</td>

                            </tr>
                            <tr>
                                <th>Estado</th>
                                <td>{{ $contrato->estado->estado }}</td>

                            </tr>
                            <tr>
                                <th>Solicitante</th>
                                <td>{{ $contrato->solicitante->nombre }}</td>

                            </tr>
                            <tr>
                                <th>Objeto</th>
                                <td>{{ $contrato->objeto->descripcion }}</td>

                            </tr>
                            <tr>
                                <th>Persona</th>
                                <td>{{ $contrato->user->name }}</td>

                            </tr>

                        </thead>

                    </table>
                </div>
            </div>
        </div>

    </div>


</div>

