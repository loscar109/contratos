@if (Session::has('delete_contrato_error'))
        <div class="alert alert-danger"data-auto-dismiss role="alert">{{ Session::get('delete_contrato_error') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif


@if (Session::has('delete_contrato'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('delete_contrato') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('store_contrato'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('store_contrato') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('update_contrato'))
        <div class="alert alert-warning"data-auto-dismiss role="alert">{{ Session::get('update_contrato') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif
