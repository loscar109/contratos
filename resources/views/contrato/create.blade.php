@extends('layouts.admin')
@section('titulo')
<div class="box-header" style="text-align:center">
    <a href="{{ asset('contrato') }}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>
</div>
@endsection

@section('content')


{!!Form::open(array(
    'url'=>'contrato',
    'method'=>'POST',
    'autocomplete'=>'off',
    'files' => true,
    'enctype'=>'multipart/form-data',
))!!}

{{Form::token()}}
<div class="box-body">
    @include('errors.request')
    @include('contrato.mensaje')
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:#D2D6DE">

        <div class="box">
            <div class="box-header">
                <h4 class="box-title" >
                    <i class="fa fa-handshake" aria-hidden="true"></i> Crear un contrato
                </h4>
            </div>
            <div class="box-body">
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="fecha_desde">
                                Fecha desde
                        </label>
                        <input
                            type="date"
                            id="fecha_desde"
                            name="fecha_desde"
                            value="{{old('fecha_desde')}}"
                            class="form-control"
                            placeholder="dd/mm/YYYY..."
                            min="2020-01-01"
                            title="Introduzca una fecha desde para el contrato"
                            required
                            >
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="fecha_hasta">
                                Fecha hasta
                        </label>
                        <input
                            type="date"
                            id="fecha_hasta"
                            name="fecha_hasta"
                            value="{{old('fecha_hasta')}}"
                            class="form-control"
                            placeholder="dd/mm/YYYY..."
                            min="2020-01-01"
                            title="Introduzca una fecha hasta para el contrato"
                            required
                            >
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="fecha_disp">
                                Fecha de disposición
                        </label>
                        <input
                            type="date"
                            name="fecha_disp"
                            value="{{old('fecha_disp')}}"
                            class="form-control"
                            placeholder="dd/mm/YYYY..."
                            min="2020-01-01"
                            title="Introduzca una fecha de disposición para el contrato"
                            required
                            >
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="nro_disp">
                                Número de disposición
                        </label>
                        <input
                            type="number"
                            name="nro_disp"
                            value="{{old('nro_disp')}}"
                            class="form-control"
                            placeholder="Ingrese un número de disposición..."
                            title="Introduzca una número para el contrato"
                            required
                            >
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="resumen">
                                Resumen
                        </label>
                        <input
                            type="string"
                            name="resumen"
                            maxlength="30"
                            value="{{old('resumen')}}"
                            class="form-control"
                            placeholder="dscriba algún resumen..."
                            title="Introduzca una resumen para el contrato"
                            required
                            >
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="nombre_archivo">
                            Nombre de archivo
                        </label>
                        <input
                            type="string"
                            name="nombre_archivo"
                            maxlength="30"
                            value="{{old('nombre_archivo')}}"
                            class="form-control"
                            placeholder="nombre de archivo..."
                            title="Introduzca un nombre de archivo para el contrato"
                            required
                            >
                    </div>
                </div>




                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="contrato">
                                Contrato
                        </label>
                        <input
                            type="text"
                            name="contrato"
                            maxlength="30"
                            value="{{old('contrato')}}"
                            class="form-control"
                            placeholder="Describa el contrato..."
                            title="Introduzca un contrato"
                            required
                            >
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="estado_id">
                            Estado
                        </label>
                        <select
                            name="estado_id"
                            id="estado_id"
                            class="estado_id form-control"
                            required
                            >
                            <option
                                value="0"
                                disabled="true"
                                selected="true"
                                title="-Seleccione un estado-"
                                >
                                -Seleccione un estado-
                            </option>
                            @foreach ($estados as $estado)
                                <option
                                    value="{{$estado->id }}">{{$estado->estado}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="solicitante_id">
                            Solicitante
                        </label>
                        <select
                            name="solicitante_id"
                            id="solicitante_id"
                            class="solicitante_id form-control"
                            required
                            >
                            <option
                                value="0"
                                disabled="true"
                                selected="true"
                                title="-Seleccione un solicitante-"
                                >
                                -Seleccione un solicitante-
                            </option>
                            @foreach ($solicitantes as $solicitante)
                                <option
                                    value="{{$solicitante->id }}">{{$solicitante->nombre}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="objeto_id">
                            Objeto
                        </label>
                        <select
                            name="objeto_id"
                            id="objeto_id"
                            class="objeto_id form-control"
                            required
                            >
                            <option
                                value="0"
                                disabled="true"
                                selected="true"
                                title="-Seleccione un objeto-"
                                >
                                -Seleccione un objeto-
                            </option>
                            @foreach ($objetos as $objeto)
                                <option
                                    value="{{$objeto->id }}">{{$objeto->descripcion}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="user_id">
                            Persona
                        </label>
                        <select
                            name="user_id"
                            id="user_id"
                            class="user_id form-control"
                            required
                            >
                            <option
                                value="0"
                                disabled="true"
                                selected="true"
                                title="-Seleccione un persona-"
                                >
                                -Seleccione una persona-
                            </option>
                            @foreach ($users as $user)
                                <option
                                    value="{{$user->id }}">{{$user->apellido}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group {{ $errors->has('anexo') ? 'has-error' : '' }} ">
                        <label for="anexo">Anexo</label>
                            <input
                                id="file"
                                type="file"
                                name="anexo"
                                class="img-responsive"

                                >
                    </div>
                    <hr>
                    <div id="preview"></div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <button title="Guardar" class="btn btn-primary btn-responsive" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                        <button title="Limpiar" class="btn btn-danger btn-responsive" type="reset"><i class="fa fa-remove"></i> Cancelar</button>
                    </div>
                </div>

             </div>
            </div>
        </div>
    </div>
    {!!Form::close()!!}

    @push('scripts')
    <script type="text/javascript">
        document.getElementById("file").onchange = function(e) {
            let reader = new FileReader();

            reader.onload = function(){
                let preview = document.getElementById('preview'),
                image = document.createElement('img');

                image.src = reader.result;

                preview.innerHTML = '';
                preview.append(image);
            };

            reader.readAsDataURL(e.target.files[0]);
        }



        $(document).ready(function(){

            $("#estado_id").select2({
                placeholder:'-Seleccione un estado-',
                width: '100%',
            });

            $("#solicitante_id").select2({
                placeholder:'-Seleccione un solicitante-',
                width: '100%',
            });

            $("#objeto_id").select2({
                placeholder:'-Seleccione un objeto-',
                width: '100%',
            });


            $("#user_id").select2({
                placeholder:'-Seleccione una persona-',
                width: '100%',
            });

            //si existe un cambio en Fecha Desde
            $('#fecha_desde').change(function() {

            //Se captura su valor
            var desde = $(this).val();
            console.log(desde, 'Se cambio la fecha DESDE')
            //y establesco ese valor capturado como minimo en Fecha Hasta
            $('#fecha_hasta').attr({"min" : desde});;


            });

            //si existe un cambio en Fecha Hasta
            $('#fecha_hasta').change(function() {

            //Se captura su valor
            var hasta = $(this).val();
            console.log(hasta, 'Se cambio la fecha HASTA');





            });


    });

</script>
@endpush

@endsection

