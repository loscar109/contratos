@extends('layouts.admin')

@section('titulo')
<div class="box-header" style="text-align:center">
    <a href="{{ asset('contrato') }}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>
</div>
@endsection

@section('content')

    {!!Form::model($contrato, [
        'method'=>'PATCH',
        'route'=>['contrato.update',$contrato->id]
    ])!!}


    {{Form::token()}}
    <div class="box-body">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:#D2D6DE">
            @include('errors.request')
            @include('contrato.mensaje')
            <div class="box">
                <div class="box-header">
                    <h4 class="box-title" >
                        <i class="fa fa-id-card" aria-hidden="true"></i> Editar un contrato
                    </h4>
                </div>
                <div class="box-body">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                            <label for="fecha_desde">
                                    Fecha desde
                            </label>
                            <input
                                type="date"
                                name="fecha_desde"
                                value="{{ $contrato->fecha_desde }}"
                                class="form-control"
                                title="Fecha desde del contrato"
                                min="2020-01-01"

                                >
                        </div>
                    </div>

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                            <label for="fecha_hasta">
                                    Fecha hasta
                            </label>
                            <input
                                type="date"
                                name="fecha_hasta"
                                value="{{ $contrato->fecha_hasta }}"
                                class="form-control"
                                title="Fecha hasta del contrato"
                                min="2020-01-01"

                                >
                        </div>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                            <label for="fecha_disp">
                                    Fecha de disposición
                            </label>
                            <input
                                type="date"
                                name="fecha_disp"
                                value="{{ $contrato->fecha_disp }}"
                                class="form-control"
                                title="Fecha disposición del contrato"
                                min="2020-01-01"

                                >
                        </div>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                            <label for="nro_disp">
                                    Número de disposición
                            </label>
                            <input
                                type="number"
                                name="nro_disp"
                                value="{{ $contrato->nro_disp }}"
                                class="form-control"
                                title="número de disposición del contrato"
                                >
                        </div>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                            <label for="resumen">
                                Resumen
                            </label>
                            <input
                                type="string"
                                name="resumen"
                                maxlength="30"
                                value="{{ $contrato->resumen }}"
                                class="form-control"
                                title="resumen del contrato"
                                >
                        </div>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                            <label for="nombre_archivo">
                                Nombre Archivo
                            </label>
                            <input
                                type="string"
                                name="nombre_archivo"
                                maxlength="30"
                                value="{{ $contrato->nombre_archivo }}"
                                class="form-control"
                                title="nombre de archivo del contrato"
                                >
                        </div>
                    </div>

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                            <label for="anexo">
                                    Anexo
                            </label>
                            <input
                                type="file"
                                name="anexo"
                                value="{{ $contrato->anexo }}"
                                class="form-control"
                                title="anexo del contrato"
                                >
                        </div>
                    </div>

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                            <label for="contrato">
                                    Contrato
                            </label>
                            <input
                                type="string"
                                name="contrato"
                                value="{{ $contrato->contrato }}"
                                class="form-control"
                                title="el contrato"
                                >
                        </div>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                            <label for="estado">
                                Estado
                            </label>
                            <select
                                id="estado"
                                name="estado_id"
                                class="form-control">
                                    @foreach ($estado as $e)
                                        @if ($e->id==$contrato->estado_id)
                                            <option value="{{$e->id}}" selected>{{$e->estado}}</option>
                                        @else
                                             <option value="{{$e->id}}">{{$e->estado}}</option>
                                        @endif
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                            <label for="solicitante">
                                Solicitante
                            </label>
                            <select
                                id="solicitante"
                                name="solicitante_id"
                                class="form-control">
                                    @foreach ($solicitante as $s)
                                        @if ($s->id==$contrato->solicitante_id)
                                            <option value="{{$s->id}}" selected>{{$s->nombre}}</option>
                                        @else
                                             <option value="{{$s->id}}">{{$s->nombre}}</option>
                                        @endif
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                            <label for="objeto">
                                Objeto
                            </label>
                            <select
                                id="objeto"
                                name="objeto_id"
                                class="form-control">
                                    @foreach ($objeto as $o)
                                        @if ($o->id==$contrato->objeto_id)
                                            <option value="{{$o->id}}" selected>{{$o->descripcion}}</option>
                                        @else
                                             <option value="{{$o->id}}">{{$o->descripcion}}</option>
                                        @endif
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                            <label for="user_id">
                                Persona
                            </label>
                            <select
                                id="user_id"
                                name="user_id"
                                class="form-control">
                                    @foreach ($user as $u)
                                        @if ($u->id==$contrato->user_id)
                                            <option value="{{$u->id}}" selected>{{$u->name}}</option>
                                        @else
                                             <option value="{{$u->id}}">{{$u->name}}</option>
                                        @endif
                                    @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                        <div class="form-group">
                            <button title="Guardar" class="btn btn-primary btn-responsive" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                            <button title="Limpiar" class="btn btn-danger btn-responsive" type="reset"><i class="fa fa-remove"></i> Cancelar</button>
                        </div>
                    </div>

                 </div>
                </div>
            </div>
        </div>
        {!!Form::close()!!}




    @endsection
