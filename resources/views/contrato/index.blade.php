@extends('layouts.admin')
@section('titulo')
<div class="box-header" style="text-align:center">
    <a href="{{ asset('/') }}">
        <button title="atras" class="btn btn-default btn-responsive pull-left">
            <i class="fa fa-arrow-left"></i> Atras
        </button>
    </a>
</div>
@endsection

@section('content')
    <div class="box-body">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:#D2D6DE">
            @include('errors.request')
            @include('contrato.mensaje')
            <div class="box">
                <div class="box-header">
                    <h4 class="box-title" >
                        <i class="fa fa-handshake" aria-hidden="true"></i> Indice de Contratos
                    </h4>
                    <div class="box-tools">
                        <a href= {{ route('contrato.create')}}>
                            <button class="btn btn-warning">
                                <i class="fa fa-plus"></i> Nuevo
                            </button>
                        </a>

                    </div>
                </div>
                <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="box collapsed-box">
                            <div class="box-header with-border">
                                <i class="fa fa-filter" aria-hidden="true"></i><h3 class="box-title">Filtrar</h3>

                              <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="desplegar">
                                  <i class="fa fa-plus"></i></button>
                              </div>
                            </div>
                            <div class="box-body" style="display: none;">

                                @include('contrato.search') <!-- elemento de la -->



                            </div>

                        </div>


                        <table id="tablaDetalle" style="border:1px solid black; width:100%" class="table table-bordered table-condensed table-hover">
                            <thead style="background-color:#222D32">
                                <tr>
                                    <th width="5%" style="color:#F8F9F9" height="15px"><p class="text-uppercase" style="font-size:120%">NroContrato</p></th>
                                    <th width="10%" style="color:#F8F9F9" height="15px"><p class="text-uppercase" style="font-size:120%">Resumen</p></th>
                                    <th width="5%" style="color:#F8F9F9" height="15px"><p class="text-uppercase" style="font-size:120%">Estado</p></th>
                                    <th width="15%" style="color:#F8F9F9" height="15px"><p class="text-uppercase" style="font-size:120%">Fecha de Carga</p></th>

                                    <th width="10%" style="color:#F8F9F9" height="15px"><p class="text-uppercase" style="font-size:120%">Opciones</p></th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($contratos as $contrato)
                                <tr onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)">
                                    <td width="5%"><p style="font-size:120%">{{ $contrato->id }}</p></td>
                                    <td><p style="font-size:120%">{{ $contrato->contrato }}</p></td>
                                    <td width="5%">
                                        @if($contrato->estado->id==1)
                                        <p style="font-size:120%"><span class="label label-primary">{{ $contrato->estado->estado }}</span></p>
                                        @elseif($contrato->estado->id==2)
                                            <p style="font-size:120%"><span class="label label-danger">{{ $contrato->estado->estado }}</span></p>
                                        @elseif($contrato->estado->id==3)
                                        <p style="font-size:120%"><span class="label label-warning">{{ $contrato->estado->estado }}</span></p>
                                        @endif
                                    </td>
                                    <td width="15%"><p style="font-size:120%">{{Carbon\Carbon::parse($contrato->created_at)->format('d/m/Y') }}</p></td>

                                    <td style="text-align: center" colspan="3">
                                        <a data-keyboard="false" data-target="#modal-delete-{{ $contrato->id }}" data-toggle="modal">
                                            <button title="eliminar" class="btn btn-warning btn-responsive">
                                                <i class="fa fa-trash"></i> Eliminar
                                            </button>
                                        </a>
                                        <a href="{{URL::action('ContratoController@edit',$contrato->id)}}">
                                            <button title="editar" class="btn btn-warning btn-responsive">
                                                <i class="fa fa-edit"></i> Modificar
                                            </button>
                                        </a>
                                        <a data-keyboard="false" data-target="#modal-show-{{ $contrato->id }}" data-toggle="modal">
                                            <button title="editar" class="btn btn-warning btn-responsive">
                                                <i class="fa fa-eye"></i> Detalle
                                            </button>

                                        </a>

                                        @include('contrato.modalshow')


                                    </td>
                                </tr>
                                @include('contrato.modaldelete')

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
    <script src="{{asset('js/tablaDetalle.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){

            $("#estado_id").select2({
                placeholder:'-Seleccione un estado-',
                width: '100%',
            });
        });
    </script>
    @endpush
    @endsection

