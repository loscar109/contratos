@if (Session::has('delete_estado_error'))
        <div class="alert alert-danger"data-auto-dismiss role="alert">{{ Session::get('delete_estado_error') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif


@if (Session::has('delete_estado'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('delete_estado') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('store_estado'))
        <div class="alert alert-success"data-auto-dismiss role="alert">{{ Session::get('store_estado') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif

@if (Session::has('update_estado'))
        <div class="alert alert-warning"data-auto-dismiss role="alert">{{ Session::get('update_estado') }}
                <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                </button>
        </div>
@endif
