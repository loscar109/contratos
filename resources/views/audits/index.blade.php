@extends('layouts.admin')

@section('content')
    <div class="box-body">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:#D2D6DE">
            @include('errors.request')
            @include('audits.mensaje')
            <div class="box">
                <div class="box-header">
                    <h4 class="box-title">
                        <i class="fa fa-tasks"> </i> Indice de Auditoría
                    </h4>
                </div>
                <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="box collapsed-box">
                            <div class="box-header with-border">
                                <i class="fa fa-filter" aria-hidden="true"></i><h3 class="box-title">Filtrar</h3>

                              <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="desplegar">
                                  <i class="fa fa-plus"></i></button>
                              </div>
                            </div>
                            <div class="box-body" style="display: none;">


                               @include('audits.search')



                            </div>

                        </div>
                        @if($audits->isNotEmpty())
                            <div id="divDetalle" class="table-responsive">
                                <table id="tablaDetalle" style="border:1px solid black; width:100%" class="table table-bordered table-condensed table-hover">
                                    <thead style="background-color:#222D32">
                                        <tr>
                                            <th  style="color:#F8F9F9" height="25px"><p class="text-uppercase">Tabla</p></th>
                                            <th  style="color:#F8F9F9" height="25px"><p class="text-uppercase">ID</p></th>
                                            <th  style="color:#F8F9F9" height="25px"><p class="text-uppercase">Evento</p></th>
                                            <th  style="color:#F8F9F9" height="25px"><p class="text-uppercase">Usuario</p></th>
                                            <th  style="color:#F8F9F9" height="25px"><p class="text-uppercase">Valores anteriores</p></th>
                                            <th  style="color:#F8F9F9" height="25px"><p class="text-uppercase">Valores Nuevos</p></th>
                                            <th  style="color:#F8F9F9" height="25px"><p class="text-uppercase">Fecha</p></th>
                                            <th  style="color:#F8F9F9" height="25px"><p class="text-uppercase">Mas Detalles</p></th>


                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($audits as $a)
                                        <tr  onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)">

                                            <td>{{$a->auditable_type }}</td>
                                            <td>{{$a->auditable_id }}</td> <!-- ID del registro modificado -->
                                            <td> <!-- Evento -->
                                                @if ($a->event == 'created')
                                                    <span class="label label-success">Creación</span>
                                                @endif
                                                @if ($a->event == 'updated')
                                                <span class="label label-warning">Actualización</span>

                                                @endif
                                                @if ($a->event == 'deleted')
                                                    <span class="label label-danger">Eliminación</span>
                                                @endif
                                            </td>
                                            <td>{{$a->user->name }}</td> <!-- Usuario Responsable -->
                                            <td>
                                                <table class="table table-bordered table-condensed table-hover table-striped" style="border:3px solid #357CA5 width:100%">
                                                    @foreach($a->old_values as $attribute => $value) <!-- Valores Anteriores -->
                                                    @if ($value!= NULL)
                                                        <tr onmouseover="cambiar_color_over2(this)" onmouseout="cambiar_color_out(this)">
                                                            <td><b>{{ $attribute }}</b></td>
                                                            <td>{{ $value }}</td>
                                                        </tr>
                                                    @else
                                                        <td>No hay datos previos</td>
                                                    @endif

                                                    @endforeach

                                                </table>
                                            </td>
                                            <td>
                                                <table class="table table-bordered table-condensed table-hover table-striped" style="border:3px solid #357CA5 width:100%">
                                                    @forelse($a->new_values as $attribute => $value) <!-- Valores Nuevos -->
                                                        <tr onmouseover="cambiar_color_over2(this)" onmouseout="cambiar_color_out(this)">
                                                            <td><b>{{ $attribute }}</b></td>
                                                            <td>{{ $value }}</td>
                                                        </tr>
                                                        @empty
                                                            No hay valores nuevos
                                                    @endforelse

                                                </table>
                                            </td>
                                            <td>{{$a->created_at->format('d/m/Y h:i:s A') }}</td> <!-- Fecha -->

                                            <td style="text-align: center" colspan="1"> <!-- Opciones -->
                                                <a href="" data-target="#modal-show-{{$a->id}}" data-toggle="modal">
                                                        <button title="ver" class="btn btn-warning btn-responsive">
                                                        <i class="fa fa-eye"></i>
                                                    </button>
                                                </a>
                                            </td>

                                        </tr>
                                        @include('audits.modalshow')


                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        @else
                        <p
                            class="p-3 mb-2 bg-warning text-dark"
                            >
                            No hay registros
                        </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
    <script src="{{asset('js/tablaDetalle.js')}}"></script>


    @endpush
    @endsection
