<?php

use Illuminate\Database\Seeder;
use App\Contrato;

class ContratoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Contrato::create([
            'fecha_desde'      =>  '2020-02-01',
            'fecha_hasta'      =>  '2020-06-05',
            'fecha_disp'       =>  '2020-03-11',
            'nro_disp'         =>  '00001',
            'anexo'            =>  'contrato.jpg',
            'resumen'          =>  'Este contrato validado hasta la fecha',
            'nombre_archivo'   =>  'Contrato001',
            'contrato'         =>  'Contratos temporales incentivados',
            'estado_id'        =>  1,
            'solicitante_id'   =>  3,
            'objeto_id'        =>  1,
            'user_id'          =>  1,
            'created_at'       =>  '2020-07-01',

        ]);
    }
}
