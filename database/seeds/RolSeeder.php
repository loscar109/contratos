<?php

use Illuminate\Database\Seeder;
use App\Rol;

class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rol::create([
            'nombre'=>'Administrador',
            'descripcion'=>'Permite gestionar los parámetros de configuración'
            ]);
        Rol::create([
            'nombre'=>'Gerente',
            'descripcion'=>'Permite gestionar los usuarios y contratos'
            ]);
        Rol::create([
            'nombre'=>'Auditor',
            'descripcion'=>'Permite supervisar los registros de auditoría que se cometen'
            ]);
      
        

    }
}