<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolSeeder::class);
        $this->call(SexoSeeder::class);
        $this->call(EstadoSeeder::class);
        $this->call(ObjetoSeeder::class);
        $this->call(SolicitanteSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ContratoSeeder::class);

    }
}
