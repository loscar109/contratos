<?php

use Illuminate\Database\Seeder;
use App\Estado;

class EstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Estado::create(['estado'=>'Activo']);
        Estado::create(['estado'=>'Baja']);
        Estado::create(['estado'=>'Reservado']);
    }
}
