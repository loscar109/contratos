<?php

use Illuminate\Database\Seeder;
use App\Objeto;

class ObjetoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Objeto::create(['descripcion'=>'Dictado Clases de Grado']);
    }
}
