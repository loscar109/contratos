<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'apellido'      =>  'Villalba',
            'name'          =>  'Carlos Lisandro',
            'password'      =>  bcrypt(12345678),
            'dni'           =>  '37530135',
            'domicilio'     =>  'Calle 42 C, Itaembe Guazú',
            'email'         =>  'elcarlosvillalba@gmail.com',
            'telefono'      =>  '44876453',
            'fecha_nac'     =>  '1994-09-14',
            'cuit'          =>  '20-37530135-111',
            'sexo_id'       =>  2,
            'rol_id'        =>  1,
            'created_at'    =>  '2020-02-20',

        ]);

        User::create([
            'apellido'      =>  'Martinez',
            'name'          =>  'Miguel',
            'password'      =>  bcrypt(12345678),
            'dni'           =>  '32230235',
            'domicilio'     =>  'Av Centenario 45443',
            'email'         =>  'miguel@gmail.com',
            'telefono'      =>  '42875264',
            'fecha_nac'     =>  '1983-10-16',
            'cuit'          =>  '22-37532135-1111',
            'sexo_id'       =>  2,
            'rol_id'        =>  2,
            'created_at'    =>  '2020-06-18',


        ]);

        User::create([
            'apellido'      =>  'Lucia',
            'name'          =>  'Margaret',
            'password'      =>  bcrypt(12345678),
            'dni'           =>  '29220215',
            'domicilio'     =>  'Barrio Villa Urquiza 33221',
            'email'         =>  'margarete@gmail.com',
            'telefono'      =>  '42425224',
            'fecha_nac'     =>  '1988-11-26',
            'cuit'          =>  '21-33534165-1111',
            'sexo_id'       =>  1,
            'rol_id'        =>  3,
            'created_at'    =>  '2020-05-21',

        ]);



    }
}
