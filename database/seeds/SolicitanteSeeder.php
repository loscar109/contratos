<?php

use Illuminate\Database\Seeder;
use App\Solicitante;

class SolicitanteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Solicitante::create(['nombre'=>'Sec. Extensión']);
        Solicitante::create(['nombre'=>'Sec. Académica']);
        Solicitante::create(['nombre'=>'Sec. Administrativa']);
        Solicitante::create(['nombre'=>'Sec. Posgrado']);

    }
}
