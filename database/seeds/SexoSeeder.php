<?php

use Illuminate\Database\Seeder;
use App\Sexo;

class SexoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sexo::create(['descripcion'=>'Femenino']);
        Sexo::create(['descripcion'=>'Masculino']);
        Sexo::create(['descripcion'=>'No Declara']);

    }
}
