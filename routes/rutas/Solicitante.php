<?php

    Route::get('solicitante', 'SolicitanteController@index')->name('solicitante.index');

    Route::delete('solicitante/{solicitante}', 'SolicitanteController@destroy')->name('solicitante.destroy');

  
    Route::get('solicitante/create', 'SolicitanteController@create')->name('solicitante.create');


    Route::post('solicitante', 'SolicitanteController@store')->name('solicitante.store');

    Route::get('solicitante/{solicitante}/edit', 'SolicitanteController@edit')->name('solicitante.edit');


    Route::patch('solicitante/{solicitante}', 'SolicitanteController@update')->name('solicitante.update');
        





?>