<?php

    Route::get('objeto', 'ObjetoController@index')->name('objeto.index');

    Route::delete('objeto/{objeto}', 'ObjetoController@destroy')->name('objeto.destroy');

  
    Route::get('objeto/create', 'ObjetoController@create')->name('objeto.create');


    Route::post('objeto', 'ObjetoController@store')->name('objeto.store');

    Route::get('objeto/{objeto}/edit', 'ObjetoController@edit')->name('objeto.edit');


    Route::patch('objeto/{objeto}', 'ObjetoController@update')->name('objeto.update');
        





?>