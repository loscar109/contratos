<?php

    Route::get('user', 'UserController@index')->name('user.index');

    Route::delete('user/{user}', 'UserController@destroy')->name('user.destroy');

  
    Route::get('user/create', 'UserController@create')->name('user.create');


    Route::post('user', 'UserController@store')->name('user.store');

    Route::get('user/{user}/edit', 'UserController@edit')->name('user.edit');


    Route::patch('user/{user}', 'UserController@update')->name('user.update');
        





?>