<?php

    Route::get('contrato', 'ContratoController@index')->name('contrato.index');

    Route::delete('contrato/{contrato}', 'ContratoController@destroy')->name('contrato.destroy');

  
    Route::get('contrato/create', 'ContratoController@create')->name('contrato.create');


    Route::post('contrato', 'ContratoController@store')->name('contrato.store');

    Route::get('contrato/{contrato}/edit', 'ContratoController@edit')->name('contrato.edit');


    Route::patch('contrato/{contrato}', 'ContratoController@update')->name('contrato.update');
        





?>