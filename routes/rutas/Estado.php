<?php

    Route::get('estado', 'EstadoController@index')->name('estado.index');

    Route::delete('estado/{estado}', 'EstadoController@destroy')->name('estado.destroy');

  
    Route::get('estado/create', 'EstadoController@create')->name('estado.create');


    Route::post('estado', 'EstadoController@store')->name('estado.store');

    Route::get('estado/{estado}/edit', 'EstadoController@edit')->name('estado.edit');


    Route::patch('estado/{estado}', 'EstadoController@update')->name('estado.update');
        





?>