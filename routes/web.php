<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );

Route::middleware(['auth'])->group(function() {
    include 'rutas/Welcome.php';
    include 'rutas/Contrato.php';
    include 'rutas/Estado.php';
    include 'rutas/Sexo.php';
    include 'rutas/Solicitante.php';
    include 'rutas/User.php';
    include 'rutas/Objeto.php';
    include 'rutas/Audit.php';

});

